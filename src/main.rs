use console_log::init_with_level;
use log::Level;
use sycamore::prelude::*;

fn main() {
    let _ = init_with_level(Level::Info);

    sycamore::render(|cx| {
        let counter = create_signal(cx, 0);
        let increment = move |_| counter.set_fn(|val| val + 1);
        let decrement = move |_| counter.set_fn(|val| val - 1);

        // body()
        //     .c(button().on(click,increment).t("Decrement"))
        //     .dyn_t(move || counter.get().to_string())
        //     .c(button().on(click, decrement).t("Increment"))
        //     .view(cx)
        //
        //     The same as:
        view! { cx,
            div(class="bg-green-100") {
                // button(on:click=decrement) { "Decrement" }
                div(on:click=decrement) { "Decrement" }
                (format!("Value: {}", counter.get()))
                // button(on:click=increment) { "Increment" }
                div(on:click=increment) { "Increment" }
            }
        }
    });
}
